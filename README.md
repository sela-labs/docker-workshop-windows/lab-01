# Docker Workshop - Windows
Lab 01: Building your first image

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir lab-01
$ cd lab-01
```

## Instructions

 - Create a new file called "default.aspx" with the content below:
```
<%@ Import Namespace="System" %>
<%@ Page Language="c#"%>

<script runat="server">
    public string GetMachineName()
    {
        return Environment.MachineName;
    }
</script>

<html>
    <head>
        <style>
        body {
        	background-image: linear-gradient(-74deg, transparent 90%, rgba(255, 255, 255, 0.23) 20%), linear-gradient(-74deg, transparent 83%, rgba(255, 255, 255, 0.18) 15%), linear-gradient(-74deg, transparent 76%, rgba(255, 255, 255, 0.1) 15%), linear-gradient(to top, #127ab1, #1799e0, #1796db);
    		background-size: cover;
    		margin-bottom: 0px!important;
        }
        div{
            font-family: 'Geomanist', sans-serif;
  			font-weight: normal;
  			color: white;
            width: 85%;
            margin: 0 auto;
            position: relative;
            margin-top: 180px;
            transform: translateY(-50%);
        }
        h1{
            font-size: 50pt
        }
        h2{
            font-size: 28pt
        }
        </style>
    </head>

    <body>
        <div>
            <h1>Hello from <% =GetMachineName() %>!</h1>
        </div>
    </body>

</html>
```

 - Create the "Dockerfile" with the content below:
```
FROM microsoft/aspnet
COPY default.aspx C:/inetpub/wwwroot
```

 - Build the image using the command:
```
$ docker build -t aspnet:1.0 .
```

 - Ensure the image was created:
```
$ docker images
```

 - Run the created image in interactive mode to attach to the container process:
```
$ docker run -it -p 5000:80 --name aspnet aspnet:1.0
```

 - Browse to the port 5000 to check if you can reach the application:
```
http://server-ip:5000
```

 - Exit from the container using:
```
$ CTRL + C
```

 - Remove the created container:
```
$ docker rm -f aspnet
```

 - As you may have noticed we're not getting any IIS logs from the container
 